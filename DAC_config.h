/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 6th, 2019                		*/
/*   Version :          V01	                                */
/*                                                          */
/* ******************************************************** */

#ifndef DAC_CONFIG_H_
#define DAC_CONFIG_H_

#define DAC_u8_NO_OF_DACs				(u8)1



DAC_ConfigurationsType DAC_AS_DACs[DAC_u8_NO_OF_DACs]=
{
		{
				DAC_u8_8_BIT_RESOLUTION,
				{DIO_u8_PIN16, DIO_u8_PIN17, DIO_u8_PIN18, DIO_u8_PIN19,
				 DIO_u8_PIN20, DIO_u8_PIN21, DIO_u8_PIN22, DIO_u8_PIN23},
				 DAC_u8_MAX_VOLTAGE_5
		}
};



#endif /* DAC_CONFIG_H_ */
