/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 6th, 2019                		*/
/*   Version :          V01	                                */
/*                                                          */
/* ******************************************************** */

#ifndef DAC_INTERFACE_H_
#define DAC_INTERFACE_H_


void DAC_voidInit(void);
u8 DAC_u8SetOutputVoltage(u8 Copy_u8DACIndex, u16 Copy_u16RequiredVoltagemV);


#endif /* DAC_INTERFACE_H_ */
