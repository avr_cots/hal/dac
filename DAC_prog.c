/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 6th, 2019                		*/
/*   Version :          V01	                                */
/*                                                          */
/* ******************************************************** */

#include "BIT_CALC.h"
#include "STD_TYPES.h"

#include "DIO_interface.h"

#include "DAC_interface.h"
#include "DAC_private.h"
#include "DAC_config.h"

static f32 DAC_f32Step[DAC_u8_NO_OF_DACs];
static u32 DAC_u32Levels[DAC_u8_NO_OF_DACs];


void DAC_voidInit(void)
{
	u8 Local_u8OuterLoopIndex = 0, Local_u8InnerLoopIndex = 0;
	u32 Local_u8TemporaryProduct = 1;

	for(Local_u8OuterLoopIndex = 0; Local_u8OuterLoopIndex < DAC_u8_NO_OF_DACs; Local_u8OuterLoopIndex++)
	{
		/* Calculating 2*Resolution */
		for(Local_u8InnerLoopIndex = 0; Local_u8InnerLoopIndex < DAC_AS_DACs[Local_u8OuterLoopIndex].Resolution; Local_u8InnerLoopIndex++)
		{
			Local_u8TemporaryProduct = Local_u8TemporaryProduct * 2;
		}
		DAC_u32Levels[Local_u8OuterLoopIndex] = Local_u8TemporaryProduct;

		/* Calculating the step of the DAC */
		DAC_f32Step[Local_u8OuterLoopIndex] = DAC_AS_DACs[Local_u8OuterLoopIndex].MaximumVoltage/ (DAC_u32Levels[Local_u8OuterLoopIndex] * 1.0);
	}

	return;
}
u8 DAC_u8SetOutputVoltage(u8 Copy_u8DACIndex, u16 Copy_u16RequiredVoltagemV)
{
	u8 Local_u8Error = STD_u8_ERROR, Local_u8LoopIndex = 0;
	u16 Local_u8OutputDigitalVoltage;

	f32 Local_u32RequiredVoltageV = Copy_u16RequiredVoltagemV/1000.0;

	if(Local_u32RequiredVoltageV < DAC_AS_DACs[Copy_u8DACIndex].MaximumVoltage)
	{

		/* Calculating the output voltage */
		Local_u8OutputDigitalVoltage = Local_u32RequiredVoltageV/DAC_f32Step[Copy_u8DACIndex];

		/* Setting the values of the pin to achieve the required output voltage */
		for(Local_u8LoopIndex = 0; Local_u8LoopIndex< DAC_AS_DACs[Copy_u8DACIndex].Resolution; Local_u8LoopIndex++)
		{
			DIO_u8SetPinValue(DAC_AS_DACs[Copy_u8DACIndex].Pins[Local_u8LoopIndex], GET_BIT(Local_u8OutputDigitalVoltage, Local_u8LoopIndex));
		}

		Local_u8Error = STD_u8_OK;
	}

	return Local_u8Error;
}
