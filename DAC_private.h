/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          April 6th, 2019                		*/
/*   Version :          V01	                                */
/*                                                          */
/* ******************************************************** */

#ifndef DAC_PRIVATE_H_
#define DAC_PRIVATE_H_


#define DAC_u8_8_BIT_RESOLUTION			(u8)8
#define DAC_u8_10_BIT_RESOLUTION		(u8)10
#define DAC_u8_16_BIT_RESOLUTION		(u8)16

#define DAC_u8_MAX_VOLTAGE_5			(u8)5


typedef struct
{
	u8 Resolution;
	u8 Pins[16];
	u8 MaximumVoltage;
}DAC_ConfigurationsType;

#endif /* DAC_PRIVATE_H_ */
